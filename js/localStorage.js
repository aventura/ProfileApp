    /*Funcion de Capturar, Almacenar datos y Limpiar campos*/
    $(document).ready(function () {
      $('#boton-guardar').click(function () {
        /*Captura de datos escrito en los inputs*/
        var nombre = document.getElementById("nombretxt").value;
        var email = document.getElementById("emailtxt").value;
        var edad = document.getElementById("edadtxt").value;
        var tCredito = document.getElementById("tarjetaCreditotxt").value;
        var fechaVenc = document.getElementById("fechaVencimientotxt").value;
          
          
        /*Guardando los datos en el LocalStorage*/
        localStorage.setItem("Nombre", nombre);
        localStorage.setItem("Email", email);
        localStorage.setItem("Edad", edad);
        localStorage.setItem("TCredito", tCredito);
        localStorage.setItem("FechaVencimiento", fechaVenc);  
          
        /*Limpiando los campos o inputs*/
        document.getElementById("nombretxt").value = "";
        document.getElementById("emailtxt").value = "";
        document.getElementById("edadtxt").value = "";
        document.getElementById("tarjetaCreditotxt").value = "";
        document.getElementById("fechaVencimientotxt").value = "";
      });
    });

    /*Funcion Cargar y Mostrar datos*/
    $(document).ready(function () {
      $('#boton-cargar').click(function () {
        /*Obtener datos almacenados*/
        var nombrex = localStorage.getItem("Nombre");
        var emailx = localStorage.getItem("Email");
        var edadx = localStorage.getItem("Edad");
        var tCreditox = localStorage.getItem("TCredito");
        var fechaVencx = localStorage.getItem("FechaVencimiento");
        /*Mostrar datos almacenados*/
        document.getElementById("nombre").innerHTML = nombrex;
        document.getElementById("email").innerHTML = emailx;
        document.getElementById("edad").innerHTML = edadx;
        document.getElementById("tarjetaCredito").innerHTML = tCreditox;
        document.getElementById("fechaVencimiento").innerHTML = fechaVencx;
      });
    });

